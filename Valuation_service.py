#!/usr/bin/env python3.5

import csv
import copy




def load(table, file_name):
    '''
    load data from file
    '''
    table.file_name = file_name
    table.fil = open(table.file_name,'rt')
    try:
        table.tab = csv.reader(table.fil)
        for row in table.tab:
                table.frame.append(row)
    finally:
            table.fil.close()
    return table.frame

def save(table, file_name):
    '''
    save data to file
    '''
    table.file_name = file_name
    table.fil = open(table.file_name,'wt')
    try:
        table.tab = csv.writer(table.fil)
        for row in table.frame:
                table.tab.writerow(row)
    finally:
            table.fil.close()


class table():
    """
    Create object for know structure column:
    ['id', 'price', 'currency', 'quantity', 'matching_id', 'total_price']
    """
    def __init__(self):

        self.frame = []

    def printTab(self):
        for row in self.frame:
            print(row)

    def calc_total_price(self, Bframe):
        '''
        Calculate total price assuming:  'total_price' = 'price' * 'quantity' * 'currency' and put as a last collumn
        '''
        p = self.frame[0].index('price')
        c = self.frame[0].index('currency')
        q = self.frame[0].index('quantity')
        self.frame[0].append('total_price')
        for row_i in self.frame[1:]:
            for row_j in Bframe.frame[1:]:
                if row_i[c] == row_j[0]:
                    #print(row_i[p],row_i[q],row_j[1])
                    self.frame[int(row_i[0])].append(float(row_i[p])*float(row_i[q])*float(row_j[1]))

    def copy(self):     
        return copy.copy(self)

    def limit_data_set(self, Bframe, Bindex, Aindex):
        """
        Limit data with another matrix:
            Bindex = 'top_priced_count'
            Aindex = 'total_price'
        """ 
        r = Bframe.frame[0].index(Bindex)
        m_id = self.frame[0].index('matching_id')
        p = self.frame[0].index(Aindex)
        frame2 = [self.frame[0]]
        
        for row_i in self.frame[1:]:
            for row_j in Bframe.frame[1:]:
                if row_i[m_id] == row_j[0]:
                    count_lim = int(row_j[r])
            size = 0

            for row_k in frame2:
                if row_k[m_id] == row_i[m_id]:
                        size+=1
            if size <= count_lim-1:
                frame2.append(row_i)
            else: # find the lowest top_price
                lowest_pos = 0
                lowest = float(row_i[p])
                for l in range(1,len(frame2)):
                    
                    if frame2[l][m_id] == row_i[m_id] and lowest > float(frame2[l][p]):
                        lowest = float(frame2[l][p])
                        lowest_pos = l
                # and substitute   
                frame2[lowest_pos] = row_i

        self.frame = frame2
        
    def limit_data_set_and_agregate(self, Bframe, Bindex, Aindex):
        """
        limit data set by top_priced_count and aggregate prices.
        Bindex = 'top_priced_count'
        Aindex = 'total_price'
        """ 
        r = Bframe.frame[0].index(Bindex)
        m_id = self.frame[0].index('matching_id')
        p = self.frame[0].index(Aindex)
        q = self.frame[0].index('quantity')
        c = self.frame[0].index('currency')
        frame2 = [self.frame[0]]
        ignored_product_count = [0 for i in range(len(Bframe.frame)-1)]
        for row_i in self.frame[1:]:
            for row_j in Bframe.frame[1:]:
                if row_i[m_id] == row_j[0]:
                    count_lim = int(row_j[r])
            size = 0
            for row_k in frame2:
                if row_k[m_id] == row_i[m_id]:
                        size+=1
            if size <= count_lim-1:
                frame2.append(row_i)
            else: # find the lowest 
                lowest_pos = 0
                lowest = float(row_i[p])
                for l in range(1,len(frame2)):
                    if frame2[l][m_id] == row_i[m_id] and lowest > float(frame2[l][p]):
                        lowest = float(frame2[l][p])
                        lowest_pos = l
                # and substitute  
                ignored_product_count[int(row_i[m_id])-1]+=1
                frame2[lowest_pos] = row_i

        self.frame = frame2
        """ AGREGATE """ 
        # introduced 5 columns: ['matching_id','total_price','avg_price','currency','ignored_products_count']
        llen = 0 # check length. This is need in case when matrix containing matching_id has some nonused id
        for i in range(len(Bframe.frame)):
            for j in range(len(self.frame)):
                if self.frame[j][m_id] == Bframe.frame[i][0]:
                    llen += 1
                    break
        frame2 = [[0 for x in range(5)] for y in range(llen)] 
        frame2[0] = ['matching_id','total_price','avg_price','currency','ignored_products_count']
        for row in frame2[1:]:
            row[3] = []
        for row_i in self.frame[1:]:
            frame2[int(row_i[m_id])][0] = row_i[m_id]                               # matching_id
            frame2[int(row_i[m_id])][1] += row_i[-1]                                # total_price
            frame2[int(row_i[m_id])][2] += float(row_i[q])                          # agregate total quantity for average           
            if not row_i[c] in frame2[int(row_i[m_id])][3] :
                frame2[int(row_i[m_id])][3].append(row_i[c])                        # currency
            frame2[int(row_i[m_id])][4] = ignored_product_count[int(row_i[m_id])-1] # ignored_products_count
        for row in frame2[1:]:
            row[3] = ','.join(row[3])                                               # change currency list to printable version
            row[2] = row[1]/row[2]                                                  # calculate average price

        self.frame = frame2

if __name__ == '__main__':

    A = table()
    B = table()
    C = table()
    load(A, './data.csv')
    load(B, './matchings.csv')
    load(C, './currencies.csv')
    A.printTab()
    B.printTab()
    C.printTab()
    print()
    
# Calculate total_price
    print('Calculate total price: ')
    A.calc_total_price(C)
    A.printTab()
    D = A.copy()
    print()
    
# total_price limit to top_priced_count
    print('total_price limit to top_priced_count: ')
    D.limit_data_set(B,'top_priced_count','total_price')
    D.printTab()
    print()
    
# agregate result  
    print('total_price limit to top_priced_count and agregate result: ')  
    D = A.copy()
    D.limit_data_set_and_agregate(B,'top_priced_count','total_price')
    D.printTab()
    print()
    
# save result: 'matching_id', 'total_price', 'avg_price', 'currency', 'ignored_products_count'
    save(D, 'top_products.csv')