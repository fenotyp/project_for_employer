# Tasks for employer - README


# Notes
This repository contains solution for the given problem:

- Project was build on Anaconda3/Python3.5;
- In other cimcurstances, I assume that implementation should be part of some DB engine;
- also I would continue solution in Python's Pandas library (here only a begining is whown from the reason of lack of time)
- Because there is lack of some details in the description, I decide to assume some decision. However, working in the company, I would contact with manager for claryfication and eliminate misunderstandings;

# Assumtions

What I do not know or I am not certain:

- 'price' concerns 1 'quantity' or arleady it is a 'sum'
- What exactly mean 'top_priced_count'. In this task I assume that this is the limit for the higest 'total_price'.
- I assume that all 'total_price' are converted to PLN and 'multiply' with 'quantity' such: 'total_price' = 'price' * 'quantity' * 'currency'
- During aggregation I do not know what how interprate 'currency' column. Threrefore I assume naively that I storage all currency from which total_price were obtained.


# Valuation_service.py

Valuation service

You are building a valuation service.

On the input you've got 3 files containing:
* 'data.csv' - product representation with price,currency,quantity,matching_id
* 'currencies.csv' - currency code and ratio to PLN, ie. GBP,2.4 can be converted to PLN with procedure 1 PLN * 2.4
* 'matchings.csv' - matching data matching_id,top_priced_count

Now, read all the data. From products with particular matching_id take those with the highest total price (price * quantity), limit data set by top_priced_count and aggregate prices.
Result save to 'top_products.csv' with five columns: matching_id,total_price,avg_price,currency, ignored_products_count.

Unit tests are necessary.

-----------------------------------------------------------
currencies.csv
currency,ratio
GBP,2.4
EU,2.1
PLN,1

data.csv
id,price,currency,quantity,matching_id
1,1000,GBP,2,3
2,1050,EU,1,1
3,2000,PLN,1,1
4,1750,EU,2,2
5,1400,EU,4,3
6,7000,PLN,3,2
7,630,GBP,5,3
8,4000,EU,1,3
9,1400,GBP,3,1

matchings.csv
matching_id,top_priced_count
1,2
2,2
3,3



