#!/usr/bin/env python
import pandas as pd





def find_distinct_max_value(DFrame, ColMax, ColDist):
    '''
    Function returns Frame of maximal valuesdistinct by the specified uniqe values in ColDist
    '''
    frames=DFrame.head(0)
    for i in (list(set(DFrame[ColDist]))):
        frames=frames.append(DFrame.loc[DFrame[DFrame[ColDist] == i][ColMax].argmax()])
    #result = pd.concat(frames)
    return frames



A=pd.DataFrame.from_csv('./data.csv')
B=pd.DataFrame.from_csv('./matchings.csv')
C=pd.DataFrame.from_csv('./currencies.csv')

print('data.csv')
print(A)
print()
print('matchings.csv')
print(B)
print()
print('currencies.csv')
print(C)

D=A.copy()
D.to_csv('./top_products.csv')

print("########################")

print(find_distinct_max_value(A, 'price', 'matching_id'))

# BUT WE KNOW THAT PRICE CAN BE IN DIFFERENT CURENCY
'''
[i for i in A['currency']]
C.index.get_loc('GBP')

C['ratio'][C.index.get_loc('GBP')]
'''
ff= [C['ratio'][C.index.get_loc(i)] for i in A['currency']]
A.insert(loc = 2, column='ratio', value=ff)

A.insert(loc = 5, column='total_price',value=[0 for i in range(len(A))])
A.total_price = A.price * A.quantity * A.ratio
print(A)

print('######################################')
print(find_distinct_max_value(A, 'total_price', 'matching_id'))






