#!/usr/bin/env python3.5

import unittest
import Valuation_service
from io import StringIO
import sys



class TestTable(unittest.TestCase):

    def setUp(self):
        self.held, sys.stdout = sys.stdout, StringIO()

    def test_table(self):
        self.assertEqual(Valuation_service.table().frame, [])

    def test_print(self):

        A = Valuation_service.table()
        A.printTab()
        self.assertEqual(sys.stdout.getvalue(),"")

        A = Valuation_service.table()
        A.frame = [['a',1],['b',2]]
        A.printTab()
        self.assertEqual(sys.stdout.getvalue(),"['a', 1]\n['b', 2]\n")

    def test_calc_total_price(self):

        A = Valuation_service.table()
        B = Valuation_service.table()
        A.frame = [['id', 'price', 'currency', 'quantity', 'matching_id'],
                   ['1', '0', 'GBP', '2', '3'],
                   ['2', '10', 'EU', '1', '1'],
                   ['3', '2000', 'PLN', '1', '1'],
                   ['4', '-10', 'EU', '2', '2']]
        B.frame = [['currency', 'ratio'],
                   ['GBP', '2.4'],
                   ['EU', '2.1'],
                   ['PLN', '0']]
        A.calc_total_price(B)
        self.assertEqual(A.frame[0][5],'total_price')
        self.assertEqual(A.frame[1][5],0)
        self.assertEqual(A.frame[2][5],21)
        self.assertEqual(A.frame[3][5],0)
        self.assertEqual(A.frame[4][5],-42)
        with self.assertRaises(IndexError): (A.frame[5][5])

    def test_limit_data_set(self):

        A = Valuation_service.table()
        B = Valuation_service.table()
        A.frame = [['id', 'price', 'currency', 'quantity', 'matching_id'],
                   ['1', '1', 'GBP', '2', '1'],
                   ['2', '2', 'EU', '1', '1'],
                   ['3', '3', 'PLN', '1', '1'],
                   ['4', '-10', 'EU', '2', '2']]
        B.frame = [['matching_id', 'top_priced_count'],
                   ['1', '2'],
                   ['2', '1'],
                   ['3', '3']]
        A.limit_data_set(B,'top_priced_count','price')
        self.assertEqual(A.frame[0][1],'price')
        self.assertEqual(A.frame[1][1],'3')
        self.assertEqual(A.frame[2][1],'2')
        self.assertEqual(A.frame[3][1],'-10')
        with self.assertRaises(IndexError): (A.frame[4][1])
        
    def test_limit_data_set_and_agregate(self):

        A = Valuation_service.table()
        B = Valuation_service.table()
        A.frame = [['id', 'price', 'currency', 'quantity', 'matching_id', 'total_price'],
                   ['1', '1', 'GBP', '2', '1', 1],
                   ['2', '2', 'EU', '1', '1', 2],
                   ['3', '3', 'PLN', '1', '1', 3],
                   ['4', '-10', 'EU', '2', '2', 4]]
        B.frame = [['matching_id', 'top_priced_count'],
                   ['1', '2'],
                   ['2', '1'],
                   ['3', '3']]
        A.limit_data_set_and_agregate(B,'top_priced_count','price')
        self.assertEqual(A.frame[0],['matching_id','total_price','avg_price','currency','ignored_products_count'])
        self.assertEqual(A.frame[1],['1',5, 2.5,'PLN,EU',1])
        self.assertEqual(A.frame[2],['2',4, 2.0,'EU',0])
        with self.assertRaises(IndexError): (A.frame[3])  

    def tearDown(self):
        sys.stdout = self.held


if __name__ == '__main__':
    unittest.main()
